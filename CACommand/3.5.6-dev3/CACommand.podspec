Pod::Spec.new do |s|
    s.name             = 'CACommand'
    s.version          = '3.5.6-dev3'
    s.summary          = 'CA Command SFL'
    s.homepage         = 'https://sis-software.de'
    s.license          = { type: 'Private', text: '
        /* Copyright (C) SIS Software GmbH - All Rights Reserved
        * Unauthorized copying of this file, via any medium is strictly prohibited
        * Proprietary and confidential
        * Written by Tobias Franke <t.franke@sis-software.de>, April 2020
        */
    ' }
    s.author           = { 'SIS Software GmbH' => 'r.bugaian@sis-software.de' }
    s.platform         = :ios
    s.ios.deployment_target = '14.0'
    s.source           = { 
        :http => "https://gitlab.com/api/v4/projects/46860783/packages/generic/#{s.name}/#{s.version}/#{s.name}.xcframework.zip",
        :headers => ["Authorization: Basic #{ ENV['CA_SDK_POD_AUTH_TOKEN'] }"]
    }
    s.swift_version    = '5'
    s.ios.vendored_frameworks = "#{s.name}.xcframework"
    s.ios.dependency 'CACore', "#{s.version}"
    s.ios.dependency 'CryptoSwift', '1.7.1'
end
