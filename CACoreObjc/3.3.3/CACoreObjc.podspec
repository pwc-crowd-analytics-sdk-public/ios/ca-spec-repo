Pod::Spec.new do |s|
    s.name             = 'CACoreObjc'
    s.version          = '3.3.3'
    s.summary          = 'Crowd Analytics General Purpose Library'
    s.homepage         = 'https://sis-software.de'
    s.license          = { type: 'Private', text: '
        /* Copyright (C) SIS Software GmbH - All Rights Reserved
        * Unauthorized copying of this file, via any medium is strictly prohibited
        * Proprietary and confidential
        * Written by Tobias Franke <t.franke@sis-software.de>, April 2020
        */
    ' }
    s.author           = { 'SIS Software GmbH' => 'r.bugaian@sis-software.de' }
    s.source           = { 
        :http => "https://gitlab.com/api/v4/projects/46860783/packages/generic/#{s.name}/#{s.version}/#{s.name}.xcframework.zip",
        :headers => ["Authorization: Basic #{ ENV['CA_SDK_POD_AUTH_TOKEN'] }"]
    }
    s.platform         = :ios
    s.swift_versions = ['5.0']
    s.ios.deployment_target = '14.0'
    
    s.pod_target_xcconfig = {
        'PRODUCT_BUNDLE_IDENTIFIER': 'de.pwc.aimos.CACore'
    }

    s.ios.vendored_frameworks = "#{s.name}.xcframework"

    s.ios.dependency 'Polyline', '5.1.0'
    
end
